﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour
{
    [SerializeField]
    private UIController _uIController;
    [SerializeField]
    private Text _scoreText;

    private void OnEnable()
    {
        Debug.Log(_uIController.CurrentScore);
        _scoreText.text = "Your score: " + _uIController.CurrentScore;
    }
}
