﻿using maxstAR;
using UnityEngine;
using UnityEngine.UI;

public class ObjectTracker : MonoBehaviour
{
    [SerializeField]
    private Button _startButton;
    [SerializeField]
    private ImageTrackableBehaviour _imageTrackable;
    private bool hasStarted;

    private void Start()
    {
      _startButton.onClick.AddListener(StartGame);
    }

    private void Update()
    {
        if (_imageTrackable.isVisible && !hasStarted)
        {
            _startButton.gameObject.SetActive(true);
        }
    }

    private void StartGame()
    {
        hasStarted = true;
        _startButton.gameObject.SetActive(false);
        _imageTrackable.isPlaying = true;
    }
}