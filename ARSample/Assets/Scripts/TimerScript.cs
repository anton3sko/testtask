﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{
    [SerializeField]
    private Text _timerText;
    [SerializeField]
    private int _time = 30;
    [SerializeField]
    private GameObject _gameOverPanel;

    private void Start()
    {
        _timerText.text = "Time Left:" + _time;
        StartCoroutine(Timer());
    }

    IEnumerator Timer()
    {
        while (_time > 0)
        {
            _time--;
            _timerText.text = "Time Left:" + _time;
            yield return new WaitForSeconds(1f);
        }
        if(_time == 0)
        {
            Debug.Log("GameOver");
            _gameOverPanel.SetActive(true);
            Time.timeScale = 0;
        }
    }
}