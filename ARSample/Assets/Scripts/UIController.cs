﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private Button _retryButton;
    [SerializeField]
    private Button _pauseButton;
    [SerializeField]
    private Button _resumButton;
    [SerializeField]
    private Button _exitButton;
    [SerializeField]
    private Text _scoreText;
    private int _currentScore;

    public int CurrentScore
    {
        get { return _currentScore; }
    }

    public void UpdateScore()
    {
        _currentScore++;
        _scoreText.text = "Score: " + _currentScore;
    }

    private void Start()
    {
        _scoreText.text = "Score: " + _currentScore;
        AddListenners();
    }

    private void AddListenners()
    {
        _retryButton.onClick.AddListener(Retry);
        _pauseButton.onClick.AddListener(Pause);
        _resumButton.onClick.AddListener(Resum);
        _exitButton.onClick.AddListener(Exit);
    }

    private void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        AudioManager.SingletonInstance.PlaySound(AudioName.ButtonClick);
        Time.timeScale = 1;
    }

    private void Pause()
    {
        AudioManager.SingletonInstance.PlaySound(AudioName.ButtonClick);
        _resumButton.gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    private void Resum()
    {
        AudioManager.SingletonInstance.PlaySound(AudioName.ButtonClick);
        Time.timeScale = 1;
        _resumButton.gameObject.SetActive(false);
    }

    private void Exit()
    {
        AudioManager.SingletonInstance.PlaySound(AudioName.ButtonClick);
        Application.Quit();
    }
}