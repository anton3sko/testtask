﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class StarScript : MonoBehaviour
{
    [SerializeField]
    private Transform[] _points;
    [SerializeField]
    private float _speed = 0.0f;
    [SerializeField]
    private float _stopDistance = 0.0f;
    private int _currentPoint;
    [SerializeField]
    private bool isReverse;
    [SerializeField]
    private UIController _uIController;
    private void Start()
    {
        float invokeTime = UnityEngine.Random.Range(3, 20);
        InvokeRepeating("SpeedRandomizer", invokeTime,invokeTime);
        InvokeRepeating("ReverseRandomizer", invokeTime, invokeTime);
    }

    private void SpeedRandomizer()
    {
        _speed = UnityEngine.Random.Range(0.2f, 0.8f);
    }

    private void ReverseRandomizer()
    {
        isReverse = true;
    }

    private void Update()
    {
        ReverseCheck();
        Move();
    }

    private void ReverseCheck()
    {
        if (isReverse)
        {
            Array.Reverse(_points);
            isReverse = false;
        }
    }

    private void Move()
    {
        float _currentDistance = Vector3.Distance(transform.position, _points[_currentPoint].position);
        transform.position = Vector3.MoveTowards(transform.position, _points[_currentPoint].position, _speed * Time.deltaTime);
        if (_currentDistance <= _stopDistance)
        {
            _currentPoint++;
        }
        if (_currentPoint == _points.Length)
        {
            _currentPoint = 0;
        }
    }
    
    private void OnMouseUpAsButton()
    {
        Debug.Log("Click");
        AudioManager.SingletonInstance.PlaySound(AudioName.StarClick);
        gameObject.SetActive(false);
        _uIController.UpdateScore();
        Invoke("EnableGameObject", 2f);
    }

    private void EnableGameObject()
    {
        gameObject.SetActive(true);
    }
}